bl_info = {
    "name": "My Addon",
    "author": "JoshP1 <joshp1@joshlucy.info>",
    "version": (0, 2, 9),
    "blender": (3, 33, 0),
    "category": "World",
    "description": "Adds settings to make a comic with Blender",
    "warning": "",
    "doc_url": "",
    "tracker_url": "",
}

import bpy
from bpy_extras.io_utils import ImportHelper

class fullpageComicTall(bpy.types.Operator):
    bl_idname = "object.standard"
    bl_label = "American standard comic"
    bl_options = {"REGISTER", "UNDO"}
    my_enum: bpy.props.EnumProperty(
        name="em",
        description="example",
        items=[('option 1', "option 1", ""),
               ('opt2', 'option 2', ''),
               ('opt2', 'option 2', '')])

    color: bpy.props.FloatVectorProperty(name="Background Color", subtype='COLOR', default=(1.0, 1.0, 1.0), min=0.0, max=1.0)

    def execute(self, context):
        # Changes the camera angle to flat for drawing. (probably does active camera not tested on multiple)
        scene = bpy.data.scenes["Scene"]
        scene.camera.location.x = 0
        scene.camera.location.y = -12
        scene.camera.location.z = 0
        scene.camera.rotation_euler.x = 1.55
        scene.camera.rotation_euler.y = 0.0
        scene.camera.rotation_euler.z = 0.0

        # Changes the render engine. Might change this to its own operator to so you can choose
        bpy.data.scenes['Scene'].render.engine = 'BLENDER_EEVEE'

        # Delete the default cube.
        if "Cube" in bpy.data.meshes:
            mesh = bpy.data.meshes["Cube"]
            bpy.data.meshes.remove(mesh)

        # try to change the color management to standard
        bpy.context.scene.view_settings.view_transform = 'Standard'

        # Try to set viewport modes
        threeD_viewport = None

        for area in bpy.context.screen.areas:
            if area.type == 'VIEW_3D':
                for space in area.spaces:
                    if space.type == 'VIEW_3D':
                        threeD_viewport = space

        threeD_viewport.shading.type = 'MATERIAL'
        # sets the viewport world lighting to True
        threeD_viewport.shading.use_scene_world = True

        # Set the scene size to the standard American comic book size
        r = bpy.context.scene.render
        r.resolution_y = 5100
        r.resolution_x = 3300

        # Ensure 'World' exists and has nodes enabled
        if 'World' in bpy.data.worlds and bpy.data.worlds['World'].use_nodes:
            world = bpy.data.worlds['World']
            # Set the background color
            world.node_tree.nodes['Background'].inputs[0].default_value[:3] = self.color

        # Create a new grease pencil object 
        gp_data = bpy.data.grease_pencils.new("Comic_Grease_Pencil")
        gp_object = bpy.data.objects.new("Comic_Grease_Pencil_Object", gp_data)

        # Link the Grease Pencil object to the scene
        context.scene.collection.objects.link(gp_object)

        # Set the location to the world origin
        gp_object.location = (0, 0, 0)

        # Set it as the active object
        context.view_layer.objects.active = gp_object

        # Hide the 3D cursor
        bpy.context.scene.cursor.location = (0, 0, 0)

        return {'FINISHED'}

class mainPage(bpy.types.Panel):
    """Creates a Panel in the World properties window"""
    bl_label = "Comic page sizes"
    bl_idname = "WORLD_PT_comic"
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_context = "world"

    def draw(self, context):
        layout = self.layout
        layout.use_property_split = True
        layout.use_property_decorate = True

        col = layout.column(align=True)
        row = col.row()
        row.label(text="size. Press 0 to enter camera view")

        col = layout.column(align=False)
        rol = col.row(align=False)
        # Operator to set for standard 7.5" x 11" comic page
        # May also add this one to its own row.
        comic_operator = rol.operator(fullpageComicTall.bl_idname, text="Set comic dimensions")
        comic_operator.color = (1.0, 1.0, 1.0)

        if context.active_object:
            col.prop(context.active_object, "my_enum")

def menu_func(self, context):
    self.layout.operator(fullpageComicTall.bl_idname, text="Set comic dimensions").color = (1.0, 1.0, 1.0)

def register():
    bpy.utils.register_class(mainPage)
    bpy.utils.register_class(fullpageComicTall)

def unregister():
    bpy.utils.unregister_class(mainPage)
    bpy.utils.unregister_class(fullpageComicTall)

if __name__ == "__main__":
    register()
