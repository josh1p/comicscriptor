bl_info = {
    "name": "Fountain Importer",
    "version": (0, 3, 1),
    "blender": (2, 93, 0),
    "category": "Object",
}

import bpy
from bpy_extras.io_utils import ImportHelper

class OBJECT_OT_fountain_import(bpy.types.Operator, ImportHelper):
    bl_idname = "object.fountain_import"
    bl_label = "Import Fountain Script"
    bl_options = {'REGISTER', 'UNDO'}

    filename_ext = ".fountain"
    filter_glob: bpy.props.StringProperty(default="*.fountain", options={'HIDDEN'})

    def execute(self, context):
        # Ensure a file is selected
        if not self.filepath:
            self.report({'ERROR'}, "No file selected.")
            return {'CANCELLED'}

        # Read the Fountain script from the specified file
        try:
            with open(self.filepath, "r") as file:
                fountain_script = file.read()
        except Exception as e:
            self.report({'ERROR'}, f"Error reading file: {str(e)}")
            return {'CANCELLED'}

        # Split the script by character
        characters = [char.strip() for char in fountain_script.split("\n\n")]

        # Create a new collection for all imported objects
        import_collection = bpy.data.collections.new("ImportedObjects")
        bpy.context.scene.collection.children.link(import_collection)

        # Initialize Z position
        z_position = 3.75

        # Process character dialogues
        for character in characters:
            lines = character.split('\n')
            if len(lines) > 1:
                # Assuming the character's name is in the first line
                character_name = lines[0].strip()

                # Create or get the collection for the character
                character_collection = bpy.data.collections.get(character_name)
                if character_collection is None:
                    character_collection = bpy.data.collections.new(name=character_name)
                    import_collection.children.link(character_collection)

                # Create a new text object for each character dialogue
                for dialogue in lines[1:]:
                    if dialogue.strip():  # Skip empty lines
                        bpy.ops.object.text_add(location=(3, 0, z_position))
                        text_object = bpy.context.active_object
                        text_object.name = f"{character_name}_Dialog"
                        text_object.data.body = dialogue.strip()

                        # Adjust font and font size
                        text_object.data.font = bpy.data.fonts.load("/home/joshp1/Dropbox/fontz/BackIssuesBB_reg.otf")
                        text_object.data.size = 0.15  # Adjust the size as needed

                        # Set the location explicitly in the Z-axis
                        text_object.location.z = z_position

                        # Set the rotation explicitly
                        text_object.rotation_euler = (1.57079637, 0, 0)  # Adjust the rotation as needed

                        # Try to unlink it from other collections.
                        # Remove object from all collections not used in a scene
                        bpy.ops.collection.objects_remove_all()

                        # Link the text object to the character's collection
                        character_collection.objects.link(text_object)

                        # Create a new material and assign it to the text object
                        text_material = bpy.data.materials.new(name=f"{character_name}_Material")
                        text_object.data.materials.append(text_material)

                        # Change font color
                        text_material.use_nodes = False  # Disable nodes to set flat color
                        text_material.diffuse_color = (0.0, 0.0, 0.0, 1.0)  # Set RGB color (red in this case) changed to black

                        # Increment Z position for the next text object
                        z_position -= .15  # Adjust as needed based on your scene scale

        return {'FINISHED'}


def menu_func(self, context):
    self.layout.operator(OBJECT_OT_fountain_import.bl_idname, text="Import Fountain Script")

class VIEW3D_PT_fountain_import(bpy.types.Panel):
    bl_label = "Fountain Importer"
    bl_idname = "PT_fountain_import"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_category = 'Tools'
    bl_context = 'objectmode'

    def draw(self, context):
        layout = self.layout
        layout.operator(OBJECT_OT_fountain_import.bl_idname, text="Import Fountain Script")


def register():
    bpy.utils.register_class(OBJECT_OT_fountain_import)
    bpy.utils.register_class(VIEW3D_PT_fountain_import)
    bpy.types.VIEW3D_MT_mesh_add.append(menu_func)

def unregister():
    bpy.utils.unregister_class(OBJECT_OT_fountain_import)
    bpy.utils.unregister_class(VIEW3D_PT_fountain_import)
    bpy.types.VIEW3D_MT_mesh_add.remove(menu_func)

if __name__ == "__main__":
    register()
